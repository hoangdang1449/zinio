package tests.quoine;

import java.text.ParseException;
import java.util.List;
import com.sss.selenium.IFrameworkAssert;
import com.sss.selenium.IFrameworkAutomation;
import interfaces.quoine.SignInPage;
import interfaces.quoine.TradingPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ExploreTest {

    WebDriver wd;

    //Test Data--------------------------------------
    private static final String USER = "hoang.quoine@gmail.com";
    private static final String PASSWORD = "QuoineX1703";
    private static final String URL = "https://quoinex-stag-accounts.quoine.io";
    private static final String BTCQty = "0.1";
    private static final String BTCPrice = "9000";

    //Element defined---------------------------------

    @BeforeClass
    public void init() throws Exception {
        System.setProperty("webdriver.chrome.driver", "src/test/java/resources/chromedriver");
        wd = new ChromeDriver();
        wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wd.get(URL);
    }

    @Test
    public void ET1() throws Exception {
        WebElement inputUserName = wd.findElement(By.xpath("//INPUT[@id='sign-in--1--email']"));
        inputUserName.sendKeys(USER);
        WebElement inputPassWord = wd.findElement(By.xpath("//INPUT[@id='sign-in--1--password--text']"));
        inputPassWord.sendKeys(PASSWORD);
        WebElement btnSignIn = wd.findElement(By.xpath("//DIV[@class=''][text()='Sign In']"));
        btnSignIn.click();
        Thread.sleep(5000);
        WebElement btnUserDetails = wd.findElement(By.xpath("//BUTTON[@id='user--detail--button']"));
        Assert.assertTrue(btnUserDetails.isDisplayed());
    }

    @Test //(enabled = false)
    public void ET2_market_buy() throws Exception {
        WebElement btnProductSelection = wd.findElement(By.xpath("//BUTTON[@id='order-entry-0--form--body--product--input--product']"));
        btnProductSelection.click();
        Thread.sleep(5000);
        WebElement btnBTCUSB = wd.findElement(By.xpath("//BUTTON[@id='order-entry-0--form--body--product--input--product--body--Starred--quoine:BTCUSD--value']"));
        btnBTCUSB.click();
        Thread.sleep(5000);
        WebElement btnTypeMarket = wd.findElement(By.xpath("//BUTTON[@id='order-entry-0--form--body--type--market']"));
        btnTypeMarket.click();
        Thread.sleep(5000);
        WebElement inputQuantity = wd.findElement(By.xpath("//INPUT[@id='order-entry-0--form--body--others--quantity--value']"));
        inputQuantity.clear();
        inputQuantity.sendKeys(BTCQty);
        Thread.sleep(5000);
        WebElement btnBuy = wd.findElement(By.xpath("//DIV[@class=''][text()='Buy']"));
        btnBuy.click();
        WebElement btnSubmitOrder = wd.findElement(By.xpath("//BUTTON[@id='order-entry-0--confirm--submit--submit']"));
        btnSubmitOrder.click();
        Thread.sleep(5000);
        WebElement btnOrdersTab = wd.findElement(By.xpath("//SPAN[@class='lm_title'][text()='Orders ']"));
        btnOrdersTab.click();
        WebElement valOrdersDate = wd.findElement(By.xpath("//SPAN[@id='order-list-0--body--model-481--updated']"));
        String value1 = valOrdersDate.getAttribute("data-value");
        System.out.println(value1);
    }

    @Test
    public void ET3_limit_by() throws Exception {
        WebElement btnTypeLimit = wd.findElement(By.xpath("//BUTTON[@id='order-entry-0--form--body--type--limit']"));
        btnTypeLimit.click();
        Thread.sleep(5000);
        WebElement inputPrice = wd.findElement(By.xpath("//INPUT[@id='order-entry-0--form--body--others--price--value']"));
        inputPrice.sendKeys(BTCPrice);
        Thread.sleep(5000);
        WebElement inputQuantity = wd.findElement(By.xpath("//INPUT[@id='order-entry-0--form--body--others--quantity--value']"));
        inputQuantity.clear();
        inputQuantity.sendKeys(BTCQty);
        WebElement inputDisclosedQty = wd.findElement(By.xpath("//INPUT[@id='order-entry-0--form--body--others--quantity-disclose--value']"));
        inputDisclosedQty.clear();
        inputDisclosedQty.sendKeys(BTCQty);
        Thread.sleep(5000);
        WebElement btnBuy = wd.findElement(By.xpath("//DIV[@class=''][text()='Buy']"));
        btnBuy.click();
        WebElement btnSubmitOrder = wd.findElement(By.xpath("//BUTTON[@id='order-entry-0--confirm--submit--submit']"));
        btnSubmitOrder.click();
        Thread.sleep(5000);
        WebElement btnCost = wd.findElement(By.xpath("//SPAN[@class='_1HH-OeSh'][text()='Cost']"));
        btnCost.click();
        WebElement ValTakerCost = wd.findElement(By.cssSelector("#order-entry-0--form--calculator--cost--detail--taker--total"));
        String value2 = ValTakerCost.getAttribute("data-value");
        System.out.println(value2);
        
    }


}
