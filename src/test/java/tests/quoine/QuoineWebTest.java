package tests.quoine;

import actions.Common.Common;
import com.sss.selenium.IFrameworkAssert;
import com.sss.selenium.IFrameworkAutomation;
import com.sss.selenium.IFrameworkElementDefinition;
import interfaces.quoine.SignInPage;
import interfaces.quoine.TradingPage;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class QuoineWebTest {
    private static final String USER = "hoang.quoine@gmail.com";
    private static final String PASSWORD = "QuoineX1703";
    private static final String URL = "https://quoinex-stag-accounts.quoine.io";
    private static final String BTCqty = "0.1";
    private static final String BTCprice = "";



    @BeforeClass
    public void init() throws Exception {
        Common.config();
        IFrameworkAutomation.openURL(URL);
    }

    @Test
    public void TC01_Verify_that_user_can_loin_successfully() throws Exception {
        SignInPage.SignIn(USER, PASSWORD);
        IFrameworkAssert.verifyTrue(IFrameworkAutomation.isElementExists(TradingPage.btnUserDetails));
    }

    /*@Test
    public void TC02_Verify_that_user_can_place_a_limit_order() throws Exception {

    }*/

    @Test
    public void TC03_Verify_that_user_can_place_a_sell_market_order() throws Exception {
        TradingPage.PlaceBuyMarketOrder(BTCqty);
//        String abc = IFrameworkAutomation.getAttribute(By.cssSelector("#order-list-0--body--model-1040"), "text");
//        System.out.println(abc);


    }
}
