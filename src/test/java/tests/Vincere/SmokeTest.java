package tests.Vincere;

import actions.Common.Common;
import com.sss.selenium.IFrameworkAssert;
import com.sss.selenium.IFrameworkAutomation;
import com.sss.selenium.IFrameworkElementDefinition;
import interfaces.Vincere.CompaniesPage;
import interfaces.Vincere.HomePage;
import interfaces.Vincere.SignInPage;;
import oracle.jrockit.jfr.jdkevents.ThrowableTracer;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.PasswordAuthentication;

public class SmokeTest {

    // General value
    private static final String URL = "https://testprod.vincere.io/";
    private static final String USERNAME = "sysadmin@vincere.io";
    private static final String PASSWORD = "$*64PHN0EM4LTH1";

    // Add company value
    private static final String COMPANYNAME = "TC01_AUTO_COMPANY_NAME";
    private static final String COMPANYADDRESS = "21 NGUYEN TRUNG NGAN";
    private static final String COMPANYINDUSTRY = "Computer Software";
    private static final String COMPANYWEBSITE = "vincere.io";
    private static final String COMPANYSWITCHBOARD = "ABC";
    private static final String COMPANYPARENT = "Hoang Test";
    private static final String COMPANYOWNER = "Hoang Dang";
    private static final String COMPANYNOTE = "TC01_AUTO_COMPANY_NOTE";

    @BeforeClass
    public void init() throws Exception {
        Common.config();
        IFrameworkAutomation.openURL(URL);
    }

    @Test (priority = 1)
    public void TC1() throws Exception{
        SignInPage.SignIn(USERNAME, PASSWORD);
        IFrameworkAssert.verifyTrue(IFrameworkAutomation.isElementExists(HomePage.btnQuickAdd));
    }

    @Test (priority = 2)
    public void TC02() throws Exception{
        HomePage.QuickAddCompany(COMPANYNAME, COMPANYADDRESS,COMPANYINDUSTRY,COMPANYWEBSITE,COMPANYSWITCHBOARD,COMPANYPARENT,COMPANYOWNER,COMPANYNOTE);
        Thread.sleep(10000);
//        HomePage.QuickSearch("TC01");
//        IFrameworkAutomation.click(HomePage.btnComapniesSearchResult);
//        Thread.sleep(5000);
//        IFrameworkAutomation.click(HomePage.lnkCompaniesSearchResult);
        IFrameworkAutomation.click(HomePage.btnCompaniesMain);
        CompaniesPage.CompaniesSummarySearch(COMPANYNAME);
        IFrameworkAutomation.click(HomePage.lnkCompaniesSearchResult);
    }
}
