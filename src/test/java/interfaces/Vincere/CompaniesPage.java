package interfaces.Vincere;

import com.sss.selenium.IFrameworkAutomation;
import com.sss.selenium.IFrameworkElementDefinition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class CompaniesPage {
    public static By inputSummarySearch = IFrameworkElementDefinition.Xpath("//INPUT[@id='summarySearch']");
    public static By btnCompaniesActions = IFrameworkElementDefinition.Xpath("//BUTTON[@id='companyEditAction']");
    public static By lnkCompaniesSearchResult = IFrameworkElementDefinition.Xpath("//A[@href='javascript:void(0);'][text()='TC01_AUTO_COMPANY_NAME']");

    //*[@id="companyFrm"]/div/div[1]/div/ul/li[4]/a

    public static void CompaniesSummarySearch (String searchvalue) throws InterruptedException {
        IFrameworkAutomation.enter(inputSummarySearch, searchvalue);
        IFrameworkAutomation.findElement(inputSummarySearch).sendKeys(Keys.RETURN);
        Thread.sleep(5000);
    }
}
