package interfaces.Vincere;

import com.sss.iframework.driver.IFrameworkWebDriver;
import com.sss.selenium.IFrameworkAutomation;
import com.sss.selenium.IFrameworkElementDefinition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.IFactoryAnnotation;

public class HomePage {

    //Quick Add Company
    public static By btnQuickAdd = IFrameworkElementDefinition.Xpath("//SPAN[@class='title'][text()='Add']");
    public static By btnQuickAddCompany = IFrameworkElementDefinition.Xpath("//SPAN[@class='label-title'][text()='Company']");
    public static By inputCompanyName = IFrameworkElementDefinition.Xpath("//INPUT[@id='qc_companyName']");
    public static By inputCompanyAddress = IFrameworkElementDefinition.Xpath("//TEXTAREA[@id='qc_locationAddress']");
    public static By inputCompanyIndustry = IFrameworkElementDefinition.Xpath("(//INPUT[@type='text'])[2]");
    public static By inputCompanyWebsite = IFrameworkElementDefinition.Xpath("//INPUT[@id='qc_website']");
    public static By inputCompanySwitchboard = IFrameworkElementDefinition.Xpath("//INPUT[@id='qc_switchBoard']");
    public static By inputCompanyParentcompany =IFrameworkElementDefinition.Xpath("//INPUT[@id='qc_selectParentCompanyName']");
    public static By inputCompanyOwners = IFrameworkElementDefinition.Xpath("(//UL[@class='chosen-choices'])[2]");
    public static By inputCompanyNote = IFrameworkElementDefinition.Xpath("//TEXTAREA[@id='qc_companyNote']");
    public static By btnSaveQuickAddCompany = IFrameworkElementDefinition.Xpath("//BUTTON[@type='button'][text()='Save']");
    public static By btnSaveAndViewQuichAddCompany = IFrameworkElementDefinition.Xpath("//BUTTON[@type='button'][contains(text(),'Save ')]");

    //Search function
    public static By inputSearch = IFrameworkElementDefinition.Xpath("//TEXTAREA[@id='new-quick-search-input']");
    public static By btnComapniesSearchResult = IFrameworkElementDefinition.Xpath("(//SPAN[@class='title'][text()='Companies'][text()='Companies'])[2]");
    public static By btnCandidatesSearchResult = IFrameworkElementDefinition.Xpath("(//SPAN[@class='title'][text()='Candidates'][text()='Candidates'])[2]");
    public static By btnContactsSearchResult = IFrameworkElementDefinition.Xpath("(//SPAN[@class='title'][text()='Contacts'][text()='Contacts'])[2]");
    public static By btnJobsSearchResult = IFrameworkElementDefinition.Xpath("(//SPAN[@class='title'][text()='Jobs'][text()='Jobs'])[2]");
    public static By lnkCompaniesSearchResult = IFrameworkElementDefinition.Xpath("//SPAN[@class='text-ellipsis font-name'][text()='TC01_AUTO_COMPANY_NAME']");
    public static By btnCompaniesMain = IFrameworkElementDefinition.Xpath("(//A[@href='/company.do?board=0&status=0'])[1]");
    public static By btnContactsMain = IFrameworkElementDefinition.Xpath("//A[@href='/contact.do?board=1&status=0']");
    public static By btnJobsMain = IFrameworkElementDefinition.Xpath("(//A[@href='/candidateDashboard.do?tabId=0'])[1]");
    public static By btnCandidatesMain = IFrameworkElementDefinition.Xpath("//A[@href='/candidateDashboard.do?tabId=1']");
    public static By btnApplicationsMain = IFrameworkElementDefinition.Xpath("//SPAN[@class='title'][text()='Applications']");



    public static void QuickAddCompany (
            String companyname,
            String companyaddress,
            String companyindustry,
            String companywebsite,
            String companyswitchboard,
            String companyparent,
            String companyowner,
            String companynote)
            throws InterruptedException {
        IFrameworkAutomation.click(btnQuickAdd);
        IFrameworkAutomation.click(btnQuickAddCompany);
        Thread.sleep(5000);
        IFrameworkAutomation.enter(inputCompanyName, companyname);
        IFrameworkAutomation.enter(inputCompanyAddress, companyaddress);
        IFrameworkAutomation.enter(inputCompanyIndustry, companyindustry);
        IFrameworkAutomation.enter(inputCompanyWebsite, companywebsite);
        IFrameworkAutomation.enter(inputCompanySwitchboard, companyswitchboard);
        IFrameworkAutomation.enter(inputCompanyParentcompany, companyparent);
        Thread.sleep(5000);
//        IFrameworkAutomation.findElement(inputCompanyParentcompany).sendKeys(Keys.TAB);
//        IFrameworkAutomation.enter(inputCompanyOwners, companyowner);
//        Thread.sleep(5000);
//        IFrameworkAutomation.findElement(inputCompanyOwners).sendKeys(Keys.RETURN);
        IFrameworkAutomation.enter(inputCompanyNote, companynote);
        Thread.sleep(5000);
        IFrameworkAutomation.click(btnSaveQuickAddCompany);
    }

    public static void QuickSearch (String searchvalue){
        IFrameworkAutomation.enter(inputSearch, searchvalue);
        //WebElement.sendKeys(Keys.RETURN);
        //IFrameworkAutomation.enter(inputSearch, Keys.RETURN);
        IFrameworkAutomation.findElement(inputSearch).sendKeys(Keys.RETURN);
    }
}
