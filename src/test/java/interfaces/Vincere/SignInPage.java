package interfaces.Vincere;

import com.sss.selenium.IFrameworkAutomation;
import com.sss.selenium.IFrameworkElementDefinition;
import org.openqa.selenium.By;

public class SignInPage {


    public static By inputUserName = IFrameworkElementDefinition.Xpath("//INPUT[@id='login-username']");
    public static By inputPassWord = IFrameworkElementDefinition.Xpath("//INPUT[@id='login-password']");
    public static By btnSignIn = IFrameworkElementDefinition.Xpath("//BUTTON[@class='btn btn-lg btn-primary btn-block'][text()='Log in']");
    public static By btnRemember = IFrameworkElementDefinition.Xpath("//LABEL[@for='alc']");
    public static By btnForgotPassWord = IFrameworkElementDefinition.Xpath("//A[@href='forgotPassword.do'][text()='Forgot your password?']");

public static void SignIn (String username, String password){

    IFrameworkAutomation.enter(inputUserName, username);
    IFrameworkAutomation.enter(inputPassWord, password);
    IFrameworkAutomation.click(btnSignIn);
    }
}
