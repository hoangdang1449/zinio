package interfaces.quoine;

import com.sss.iframework.driver.IFrameworkWebDriver;
import com.sss.selenium.IFrameworkAssert;
import com.sss.selenium.IFrameworkAutomation;
import com.sss.selenium.IFrameworkElementDefinition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignInPage {
    public static By inputEmailAddress = IFrameworkElementDefinition.CssSelector("#sign-in--1--email");
    public static By inputPasswordAddress = IFrameworkElementDefinition.CssSelector("#sign-in--1--password--text");
    public static By btnSignIn = IFrameworkElementDefinition.CssSelector("#sign-in--1--submit");

    public static void SignIn (String username, String password) {
        IFrameworkAutomation.enter(inputEmailAddress, username);
        IFrameworkAutomation.enter(inputPasswordAddress, password);
        IFrameworkAutomation.click(btnSignIn);
    }
}

