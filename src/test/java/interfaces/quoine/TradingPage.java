package interfaces.quoine;

import com.sss.iframework.driver.IFrameworkWebDriver;
import com.sss.selenium.IFrameworkAssert;
import com.sss.selenium.IFrameworkAutomation;
import com.sss.selenium.IFrameworkElementDefinition;
import jdk.nashorn.internal.ir.IfNode;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TradingPage {
    public static By btnUserDetails = IFrameworkElementDefinition.CssSelector("#user--detail--button");
    public static By inputPrice = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--body--others--price--value");
    public static By inputQuantity = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--body--others--quantity--value");
    public static By inputDisclosedQty = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--body--others--quantity-disclose--value");
    public static By btnCostCalculator = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--calculator--cost--overview--toggle");
    public static By btnCostDisplay = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--calculator ._1q3XT1tx");
    public static By btnTakerCost = IFrameworkElementDefinition.CssSelector("#panel--order-entry-0 ._87fkEaKY");
    public static By btnMakerCost = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--calculator ._87fkEaKY");
    public static By btnSell = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--submit--buttons--sell");
    public static By btnBuy = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--submit--buttons--buy");
    public static By btnMarketType = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--body--type--market");
    public static By btnSubmitOrder = IFrameworkElementDefinition.CssSelector("#order-entry-0--confirm--submit--submit");
    public static By lstOrderList = IFrameworkElementDefinition.CssSelector("#order-entry-0--confirm--submit--submit");
    public static By lstProductPair = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--body--product--input--product");
    public static By btnProductPair = IFrameworkElementDefinition.CssSelector("#order-entry-0--form--body--product--input--product--body--Starred--quoine:BTCUSD--value");
    public static By btnTrailingStop = IFrameworkElementDefinition.Xpath("//BUTTON[@id='order-entry-0--form--body--type--trailing-stop']");

    public static void PlaceSellLimitOrder (String price, String quantity, String disclosurequantity){
        IFrameworkAutomation.enter(inputPrice, price);
        IFrameworkAutomation.enter(inputQuantity, quantity);
        IFrameworkAutomation.enter(inputDisclosedQty, disclosurequantity);
        IFrameworkAutomation.click(btnSell);
    }

    public static void PlaceBuyLimitOrder (String price, String quantity, String disclosurequantity){
        IFrameworkAutomation.enter(inputPrice, price);
        IFrameworkAutomation.enter(inputQuantity, quantity);
        IFrameworkAutomation.enter(inputDisclosedQty, disclosurequantity);
        IFrameworkAutomation.click(btnBuy);
    }

    public static void PlaceBuyMarketOrder (String quantity){
        IFrameworkAutomation.click(lstProductPair);
        IFrameworkAutomation.waitForControl(btnProductPair);
        IFrameworkAutomation.click(btnProductPair);
        IFrameworkAutomation.click(btnMarketType);
        IFrameworkAutomation.click(inputQuantity);
        IFrameworkAutomation.enter(inputQuantity, quantity);
        IFrameworkAutomation.click(btnSell);
        IFrameworkAutomation.click(btnSubmitOrder);
    }
}
