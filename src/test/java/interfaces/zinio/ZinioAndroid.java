package interfaces.zinio;

import com.sss.selenium.IFrameworkAutomation;
import interfaces.android.AndroidScreenEntity;
import org.openqa.selenium.By;

public class ZinioAndroid extends AndroidScreenEntity {

    public static By btnAccount = By
            .xpath("//android.widget.FrameLayout[@resource-id='com.zinio.mobile.android.reader:id/menu_account']");
    public static By txtUserProfile = By
            .xpath("//android.widget.TextView[@resouce-id='com.zinio.mobile.android.reader:id/tv_user_profile']");
    public static By txtEmail = By
            .xpath("//android.widget.EditText[@resource-id='com.zinio.mobile.android.reader:id/email_field']");
    public static By txtPass = By
            .xpath("//android.widget.EditText[@resource-id='com.zinio.mobile.android.reader:id/password_field']");
    public static By btnLogIn = By
            .xpath("//android.widget.Button[@resource-id='com.zinio.mobile.android.reader:id/sign_in_button']");
    public static By btnSearch = By.xpath("//android.widget.FrameLayout[@resource-id='android.widget.FrameLayout']");
    public static By txtSearch = By
            .xpath("//android.widget.EditText[@resource-id='com.zinio.mobile.android.reader:id/search_src_text']");
    public static By txtResult = By
            .xpath("//android.widget.TextView[@resource-id='com.zinio.mobile.android.reader:id/tv_publication_name']");
    public static By iconSearch = By
            .xpath("//android.widget.ImageView[@resrouce-id='com.zinio.mobile.android.reader:id/search_mag_icon']");

    public static void LogIn(String user, String pass) {
        IFrameworkAutomation.click(btnAccount);
        IFrameworkAutomation.click(txtUserProfile);
        IFrameworkAutomation.enter(txtEmail, user);
        IFrameworkAutomation.enter(txtPass, pass);
        IFrameworkAutomation.click(btnLogIn);
    }

    public static void Search(String search) {
        IFrameworkAutomation.click(btnSearch);
        IFrameworkAutomation.enter(txtSearch, search + "\n");
    }
}
